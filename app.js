const moonPath = 
  'M16 27.7206C16 42.9085 28 55.2206 28 55.2206C12.8122 55.2206 0.5 42.9085 0.5 27.7206C0.5 12.5328 12.8122 0.220626 28 0.220626C28 0.220626 16 12.5328 16 27.7206Z';

const sunpath = 
  'M55 27.5C55 42.6878 42.6878 55 27.5 55C12.3122 55 0 42.6878 0 27.5C0 12.3122 12.3122 0 27.5 0C42.6878 0 55 12.3122 55 27.5Z';

const darkMode = document.querySelector('#darkMode');
let toggle = false;

darkMode.addEventListener('click', () => {
  
  const timeline = anime.timeline({
    duration: 500,
    easing: 'easeOutExpo', 
  });

  timeline.add({
    targets: '.sun',
    d: [
      {value: toggle ? sunpath : moonPath} 
    ]
  })
  .add({
    targets: '#darkMode',
    rotate: 320
  }, '-= 350')
  .add({
    targets: '.mobile',
    color: toggle ? '#161616' : '#fffff',
    backgroundColor: toggle ? '#fffff' : '#161616'
  }, '-= 350')
  .add({
    targets: '.element',
    backgroundColor: toggle ? '#E6E7E7' : '#1A1B1B' 
  }, '-= 450')

  if(!toggle) {
    toggle = true;
  } else {
    toggle = false;
  }
});